﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using transport_task.PointsClasses;

namespace transport_task
{
    class DataProvider
    {
        public static List<TPoint> getData()
        {
            List<TPoint> data = new List<TPoint>();

            int counter = 0;
            string line;

            // Read the file and display it line by line.
            System.IO.StreamReader file =
               new System.IO.StreamReader("data\\train.txt");
            while ((line = file.ReadLine()) != null)
            {
                //Console.WriteLine(line);
                var parts = line.Split('\t');
                var point = new TPoint(int.Parse(parts[0]),
                                       double.Parse(parts[1]),
                                       double.Parse(parts[2]),
                                       parts[3]);
                data.Add(point);
                //Utils.Log(point.ToString());
                counter++;
            }

            file.Close();

            return data;
        }


        public static void cacheTPoints(List<TPoint> points, string filename) {
            using (StreamWriter w = File.AppendText(filename))
            {
                foreach (var p in points)
                {
                    w.WriteLine("{0}\t{1}\t{2}\t{3}", p.time, p.x, p.y, p.id);
                }

            }
        }


        public static List<TPoint> getTPointsFromCache(string filename)
        {
            List<TPoint> data = new List<TPoint>();

            if (!File.Exists(filename))
                return null;

            int counter = 0;
            string line;

            // Read the file and display it line by line.
            System.IO.StreamReader file =
               new System.IO.StreamReader(filename);
            while ((line = file.ReadLine()) != null)
            {
                //Console.WriteLine(line);
                var parts = line.Split('\t');
                var point = new TPoint(int.Parse(parts[0]),
                                       double.Parse(parts[1]),
                                       double.Parse(parts[2]),
                                       parts[3]);
                data.Add(point);
                //Utils.Log(point.ToString());
                counter++;
            }

            file.Close();

            return data;
        }
    }
}
