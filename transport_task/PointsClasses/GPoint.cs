﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using transport_task.PointsClasses;

namespace transport_task
{
    public class GPoint
    {
        public int x, y;
        public Color color;
        public string label;
        public int vx, vy; // vector from x, y        
        public int size;

        public GPoint(int _x = 0, int _y = 0, Color? _color = null, string _label = null,
            int _vx = 0, int _vy = 0, int _size = 1)
        {            
            init(_x, _y, _color, _label, _vx, _vy, _size);
        }

        public GPoint(double _x = 0, double _y = 0, Color? _color = null, string _label = null,
            int _vx = 0, int _vy = 0, int _size = 1)
        {
            init((int)_x, (int)_y, _color, _label, _vx, _vy, _size);
        }

        public GPoint(TPoint _tpoint, Color? _color = null, string _label = null,
            int _vx = 0, int _vy = 0, int _size = 1)
        {
            init((int)_tpoint.x, (int)_tpoint.y, _color, _label, _vx, _vy, _size);
        }

        public GPoint(Point _point, Color? _color = null, string _label = null,
            int _vx = 0, int _vy = 0, int _size = 1)
        {
            init(_point.X, _point.Y, _color, _label, _vx, _vy, _size);
        }

        private void init(int _x = 0, int _y = 0, Color? _color = null, string _label = null,
            int _vx = 0, int _vy = 0, int _size = 1)
        {
            x = _x;
            y = _y;
            color = _color.GetValueOrDefault(Color.Black);
            label = _label;
            vx = _vx;
            vy = _vy;
            size = _size;
        }

        public static GPoint operator +(GPoint a, GPoint b)
        {
            return new GPoint(a.x + b.x, a.y + b.y, a.color, a.label, a.vx+b.vx, a.vy+b.vy, a.size);
        }

        public static GPoint operator -(GPoint a)
        {
            return new GPoint(-a.x, -a.y, a.color, a.label, -a.vx, -a.vy, a.size);
        }

        public static GPoint operator -(GPoint a, GPoint b)
        {
            return a + (-b);
        }

        public static GPoint operator *(double k, GPoint a)
        {
            return new GPoint(k * a.x, k * a.y, a.color, a.label, (int)(k * a.vx), (int)(k * a.vy), a.size);
        }

        public static GPoint operator *(GPoint a, double k)
        {
            return k * a;
        }

        public static GPoint operator /(GPoint a, double k)
        {
            return 1 / k * a;
        }

        public double norm()
        {
            return Math.Sqrt(x * x + y * y);
        }


        public double vectorNorm() {
            return Math.Sqrt(vx * vx + vy * vy);
        }

        public Point ToPoint()
        {
            return new Point(x, y);
        }

        public override string ToString()
        {
            return string.Format("({0}, {1}, v:({2}, {3}))", x, y, vx, vy);
        }

    }
}
