﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace transport_task
{
    public class DotData<T>
    {
        public List<T> points;

        public DotData(List<T> _points){
            points = _points;
        }

        

        public DotData<T> filter(Func<T, bool> _check_func) {
            List<T> _filtered_points = new List<T>();
            foreach (var p in points) {
                if (_check_func(p)) {
                    _filtered_points.Add(p);
                }
            }

            return new DotData<T>(_filtered_points);
        }

        public DrawData getDrawData(Func<T, GPoint> _gpoint_generator, bool _drawVectors = false, bool _connectPoints = false) {
            List<GPoint> gpoints = new List<GPoint>();
            foreach (var p in points) {
                gpoints.Add(_gpoint_generator(p));
            }

            return new DrawData(gpoints, null, -1, _drawVectors, _connectPoints);
        }


        

    }
}
