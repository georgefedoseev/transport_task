﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace transport_task.PointsClasses
{
    class Trajectory
    {
        public List<TPoint> points;
        private double _length = -1;

        public Trajectory(List<TPoint> _points) {
            points = _points;
        }

        public double length() {

            if (_length != -1)
                return _length;

            double sum_distance = 0;

            int i = 1;
            while (i < points.Count)
            {
                sum_distance += Math.Sqrt(Math.Pow(points[i].x - points[i - 1].x, 2) + Math.Pow(points[i].y - points[i - 1].y, 2));
                i++;
            }

            _length = sum_distance;
            return sum_distance;
        }

        public TPointData getTPointData() {
            return new TPointData(points);
        }


        public TPoint pointOnDistanceFromStart(double distance) {
            double sum_distance = 0;

            int i = 1;
            while (sum_distance < distance && i < points.Count-1) {
                sum_distance += Math.Sqrt(Math.Pow(points[i].x - points[i - 1].x, 2) + Math.Pow(points[i].y - points[i - 1].y, 2));
                i++;
            }

            return points[i];
        }



    }
}
