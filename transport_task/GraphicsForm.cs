﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace transport_task
{
    public partial class GraphicsForm : Form
    {

        private volatile Func<GraphicsForm, Nothing> drawingFunc;

        public double zoom = 1;

        private const double DELTA_ZOOM = 1.2;
        private const double MAX_ZOOM = 10;
        private const double MIN_ZOOM = 0.01;

        public GPoint startPoint;
        public GPoint globalStartOffset;
        public volatile bool dragging;
        public bool mouse_inside;



        private GPoint startPoint_tmp;
        
        private GPoint dragStart;

        public Graphics gr;


        public Bitmap bitmap;
        public double bitmap_quality_zoom;

        private List<GPoint> stops = new List<GPoint>();
        private int current_stop;

        private List<GPoint> tags = new List<GPoint>();



        public GraphicsForm(Func<GraphicsForm, Nothing> _drawingFunc = null)
        {
            InitializeComponent();

            setDrawingFunction(_drawingFunc);

            // zooming and dragging
            zoom = 1;
            this.MouseWheel += onMouseWheel;

            startPoint = new GPoint(0, 0);
            this.MouseDown += onMouseDown;
            this.MouseDoubleClick += onMouseDoubleClick;
            this.MouseClick += onMouseRightClick;
            this.MouseMove += onMouseMove;
            this.MouseUp += onMouseUp;
            this.MouseLeave += onMouseLeave;           
            dragging = false;

            this.Resize += onResize;

            gr = this.CreateGraphics();

            bitmap = null;

            for (int i = 0; i < 39; i++) {
                stops.Add(new GPoint(0, 0));
            }

            current_stop = 0;

            updateStopLabel();
           
        }


        public void setDrawingFunction(Func<GraphicsForm, Nothing> _drawingFunc) {
            drawingFunc = _drawingFunc;
        }


        protected override void OnPaint(PaintEventArgs e)
        {
          

            Console.WriteLine("start point "+startPoint.ToString());
            drawBitmap();

            //drawTags();

        

            base.OnPaint(e);
        }


        public void drawCoordinates() {
            var mouse = new GPoint(this.PointToClient(Cursor.Position));
            var global_mouse = mouse / zoom - startPoint;
            var real_global_mouse = global_mouse - globalStartOffset;

            stops[current_stop] = real_global_mouse;
            // reverse y-axis
            real_global_mouse.y = (int)(1.0 / bitmap_quality_zoom*bitmap.Height) - real_global_mouse.y;

            gr.DrawString(string.Format("({0}, {1})", real_global_mouse.x, real_global_mouse.y), new Font("Helvetica", 16), Brushes.Black, mouse.ToPoint());
            updateStopLabel();
        }

        private void drawTags() {
            foreach (var tag in tags) {                
                // reverse y-axis
                var tmp_tag = new GPoint(tag.ToPoint());
                tmp_tag += globalStartOffset;
                tmp_tag += startPoint;
                tmp_tag *= zoom;
                //tmp_tag.y = (int)(1.0 / bitmap_quality_zoom * bitmap.Height) - tmp_tag.y;
                

                Console.WriteLine("Draw tag at "+tmp_tag.ToString());

                gr.FillEllipse(new Pen(Color.Red).Brush, tmp_tag.x - 5, tmp_tag.y - 5, 10, 10);
            }
        }

        public void setBitmap(Bitmap _bitmap) {
            bitmap = _bitmap;
            Console.WriteLine(string.Format("Bitmap resolution: {0}x{1}", bitmap.HorizontalResolution, bitmap.VerticalResolution));
        }

        private void drawBitmap() {
            // Create rectangle for source image.
            Rectangle srcRect = new Rectangle((int)(-startPoint.x * bitmap_quality_zoom), (int)(-startPoint.y * bitmap_quality_zoom), (int)(this.Width / zoom * bitmap_quality_zoom), (int)(this.Height / zoom * bitmap_quality_zoom));

            // Create rectangle for displaying image.
            Rectangle destRect = new Rectangle(0, 0, this.Width, this.Height);
            

            gr.DrawImage(bitmap, destRect, srcRect, GraphicsUnit.Pixel);
        }

        private void onMouseDown(object sender, MouseEventArgs e){
            var mouse = new GPoint(this.PointToClient(Cursor.Position));
            //Console.WriteLine("Mouse Down");
            dragging = true;
            dragStart = mouse;
            startPoint_tmp = startPoint;

      

            //Console.WriteLine(string.Format("GLOBAL pos: {0}", global_p));
        }

        private void onMouseDoubleClick(object sender, MouseEventArgs e)
        {
          //  this.Refresh();
            drawCoordinates();
        }

        private void onMouseRightClick(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right) {
                var mouse = new GPoint(this.PointToClient(Cursor.Position));
                var global_mouse = mouse / zoom - startPoint;
                var real_global_mouse = global_mouse - globalStartOffset;

                bool removed = false;
                var new_tags = new List<GPoint>();
                foreach (var tag in tags) {
                    if ((tag - real_global_mouse).norm() < 50) {                        
                        removed = true;
                    }else{
                        new_tags.Add(tag);
                    }
                }

                if (!removed)
                    new_tags.Add(real_global_mouse);
                tags = new_tags;

                tags_count.Text = tags.Count.ToString();

                this.Refresh();
            }
              
            
        }

        private void onMouseMove(object sender, MouseEventArgs e) {
            // Console.WriteLine("Mouse Move");

            var mouse = new GPoint(this.PointToClient(Cursor.Position));
            if(dragging){
                startPoint = startPoint_tmp - (dragStart - mouse) / zoom;
                this.Refresh();
            }
       
            mouse_inside = true;
        }

        private void onMouseUp(object sender, MouseEventArgs e) {
            
            dragging = false;
        }

        private void onMouseLeave(object sender, EventArgs e) {
            //Console.WriteLine("Mouse Leave");
            dragging = false;
            mouse_inside = false;
        }


        private void onMouseWheel(object sender, MouseEventArgs e) {
            

            var mouse = new GPoint(this.PointToClient(Cursor.Position));
            var global_mouse =  mouse/zoom - startPoint;

            if (e.Delta < 0) {
                zoom /= DELTA_ZOOM;
            }else{
                zoom *= DELTA_ZOOM;                
            }

            startPoint = mouse/zoom - global_mouse;

            if (zoom > MAX_ZOOM)
                zoom = MAX_ZOOM;
            else if (zoom < MIN_ZOOM)
                zoom = MIN_ZOOM;

            Console.WriteLine("Zoom: "+zoom);

            this.Refresh();
        }

        private void onResize(object sender, EventArgs e)
        {
            gr = this.CreateGraphics();
        }


        private GPoint getGlobalMousePos(MouseEventArgs e) {
            var mouse = new GPoint(e.X, e.Y);
            return mouse / zoom - startPoint;
        }

        private void prev_button_Click(object sender, EventArgs e)
        {
            current_stop--;
            if (current_stop < 0)
                current_stop = 0;
            updateStopLabel();
        }

        private void next_button_Click(object sender, EventArgs e)
        {
            current_stop++;
            if (current_stop > stops.Count-1)
                current_stop = stops.Count - 1;
            updateStopLabel();
        }

        private void updateStopLabel() {
            stop_number.Text = (current_stop+1).ToString();
            current_stop_coords.Text = stops[current_stop].ToString();
        }

        private void writeStopsToFile() {
            //File.Delete("stop.txt");
            using (StreamWriter w = File.AppendText("stops_"+Utils.secondsSince1970().ToString()+".txt"))
            {
                foreach (var stop in stops) {
                    w.WriteLine("{0}.0 {1}.0", stop.x, stop.y);
                }
                
            }
        }

        private void write_to_file_btn_Click(object sender, EventArgs e)
        {
            writeStopsToFile();
            MessageBox.Show("Saved!");
        }

       
        

        
    }
}

