﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using transport_task.PointsClasses;

namespace transport_task
{
    class DataManipulator
    {
        private List<TPoint> data;

        public DataManipulator(List<TPoint> _data) {
            data = _data;
        }
        
        public void run()
        {
            #region Preparation and statistics


            // labeled points
            var start_tpoint = new TPoint(0, 11038.08464497, 8253.17542416, "");
            var start_point = new GPoint(start_tpoint, Color.Black, "START");
            
            var middle_tpoint = new TPoint(0, 3425.67079005, 3469.94198377, "");
            var middle_point = new GPoint(middle_tpoint, Color.Black, "MIDDLE");

            var end_tpoint = new TPoint(0, 283.08479678, 163.45489494, "");
            var end_point = new GPoint(end_tpoint, Color.Black, "END");
           
            var labeled_points = new DrawData(new List<GPoint>() { 
                start_point,
                end_point,
                middle_point
            }, Color.Red, 5);


            // statistics
            double hour_sum = 0;
            int hour_count = 0;
            int min_hour = int.MaxValue;             
            
            foreach (TPoint p in data) {
                var dt = Utils.UnixTimeStampToDateTime(p.time);
                hour_sum += dt.Hour;
                hour_count++;

                if(dt.Hour > 3 && dt.Hour < min_hour)
                    min_hour = dt.Hour;
            }

            Console.WriteLine(string.Format("Total points: {0}", data.Count));
            Console.WriteLine("Peak hour: {0}", hour_sum/hour_count);
            Console.WriteLine("Min hour: {0}", min_hour);

            #endregion

            var busesPoints = new Dictionary<string, List<TPoint>>();

            #region Split data per vehicle
            /*
             * РАЗБИЕНИЕ ПО АВТОБУСАМ
             */

            // bus -> points dictionary
            
            foreach(var p in data){
                if (!busesPoints.ContainsKey(p.id))
                    busesPoints[p.id] = new List<TPoint>();
                busesPoints[p.id].Add(p);
            }

            Console.WriteLine("Buses count: {0}", busesPoints.Count);

            // sort by time for each bus
            foreach (var kv in busesPoints) {
                kv.Value.Sort((x, y) => x.time.CompareTo(y.time));
            }            
            #endregion

            #region Calculate TPoint parameters

            /*
             *  ВЫЧИСЛЕНИЕ СКОРОСТИ ДЛЯ КАЖДОЙ ТОЧКИ
             */

            // calculate velocities
            double velocities_sum = 0;
            int velocities_count = 0;
            double max_velocity = double.MinValue;
            
            foreach (var bus_points_kv in busesPoints) { 
                var bus_points = bus_points_kv.Value;
                for (int i = 0; i < bus_points.Count-3; i++) {
                    if (i > 0){                        
                        bus_points[i].velocity =
                             (
                                Math.Sqrt(
                                    Math.Pow((bus_points[i].x - bus_points[i - 1].x) / (bus_points[i].time - bus_points[i - 1].time), 2)
                                    + Math.Pow((bus_points[i].y - bus_points[i - 1].y) / (bus_points[i].time - bus_points[i - 1].time), 2)
                                )
                                +
                                Math.Sqrt(
                                    Math.Pow((bus_points[i+1].x - bus_points[i].x) / (bus_points[i+1].time - bus_points[i].time), 2)
                                    + Math.Pow((bus_points[i + 1].y - bus_points[i].y) / (bus_points[i + 1].time - bus_points[i].time), 2)
                                )
                            )/2;

                        bus_points[i].vel_x =
                            (                                
                             (bus_points[i].x - bus_points[i - 1].x) / (bus_points[i].time - bus_points[i - 1].time)
                            +
                             (bus_points[i + 1].x - bus_points[i].x) / (bus_points[i + 1].time - bus_points[i].time)                             
                            ) / 2;

                        bus_points[i].vel_y =
                            (
                             (bus_points[i].y - bus_points[i - 1].y) / (bus_points[i].time - bus_points[i - 1].time)
                            +
                             (bus_points[i + 1].y - bus_points[i].y) / (bus_points[i + 1].time - bus_points[i].time)                            
                            ) / 2;
                    }

                    velocities_count++;
                    velocities_sum += bus_points[i].velocity;
                    if (bus_points[i].velocity > max_velocity)
                        max_velocity = bus_points[i].velocity;
                    
                }
            }

            Console.WriteLine("Average velocity: {0}", velocities_sum / velocities_count);
            Console.WriteLine("Max velocity: {0}", max_velocity);
            #endregion

            var forward_direction = new Dictionary<string, List<TPoint>>();
            var backward_direction = new Dictionary<string, List<TPoint>>();

            #region Split directions
            /* SPLIT DIRECTIONS FOR EVERY VEHICLE */
           

            foreach (var bus_points_kv in busesPoints) { 
                var bus_points = bus_points_kv.Value;

                if (!forward_direction.ContainsKey(bus_points_kv.Key))
                    forward_direction[bus_points_kv.Key] = new List<TPoint>();

                if (!backward_direction.ContainsKey(bus_points_kv.Key))
                    backward_direction[bus_points_kv.Key] = new List<TPoint>();

                var unknown_dir_points = new List<TPoint>();

                bool reached_something = false;
                bool prev_was_start = false;
                for(int i = 1; i < bus_points.Count; i++){
                    var p = bus_points[i];
                    var gp = new GPoint(p);

                    unknown_dir_points.Add(p);

                    if((start_point-gp).norm()<100 && (!prev_was_start || !reached_something)){
                        // reached start                        
                        backward_direction[bus_points_kv.Key].AddRange(unknown_dir_points);
                        unknown_dir_points.Clear();
                        reached_something = true;
                        prev_was_start = true;
                    }

                    if ((middle_point - gp).norm() < 100 && reached_something)
                    {
                        // reached middle
                        if(prev_was_start)
                            forward_direction[bus_points_kv.Key].AddRange(unknown_dir_points);
                        else
                            backward_direction[bus_points_kv.Key].AddRange(unknown_dir_points);
                        unknown_dir_points.Clear();
                    }

                    if ((end_point - gp).norm() < 100 && (prev_was_start || !reached_something))
                    {
                        // reached end
                        forward_direction[bus_points_kv.Key].AddRange(unknown_dir_points);
                        unknown_dir_points.Clear();
                        reached_something = true;
                        prev_was_start = false;
                    }
                }
            }
            #endregion

            double VELOCITY_THRESHOLD_MIN = 3.5;
            double VELOCITY_THRESHOLD_MAX = 6;
            var frwd_bp_vel = new Dictionary<string, List<TPoint>>();

            #region Select particular velocity
            /* 
             * SELECT VELOCITY 
             */
            
            foreach (var bus_points_kv in forward_direction)
            {
                var bus_points = bus_points_kv.Value;
                var new_bus_points = new List<TPoint>();
                foreach (var p in bus_points)
                {
                    if (p.velocity > VELOCITY_THRESHOLD_MIN && p.velocity < VELOCITY_THRESHOLD_MAX)
                        new_bus_points.Add(p);
                }
                frwd_bp_vel[bus_points_kv.Key] = new_bus_points;
            }
            #endregion

            #region Flatting data
            /*
            * FLAT DATA
            */

            // raw flat            
            var raw_flat = new TPointData(data);

            // frwd flat            
            var frwd_flat = flattenBusesData(forward_direction);

            // bkwd flat            
            var bkwd_flat = flattenBusesData(backward_direction);

           // frwd velocity flat
            var frwd_flat_vel = flattenBusesData(frwd_bp_vel);

            frwd_flat_vel = frwd_flat;
            #endregion

            /* NOISE REDUCE */

            #region Long stay points
            // ВЫЧИСЛЕНИЕ ТОЧЕК ДОЛГОЙ СТОЯНКИ
            var long_stay_bp = new Dictionary<string, List<TPoint>>();
            foreach (var bus_points_kv in busesPoints)
            {
                var bus_points = bus_points_kv.Value;
                var new_bus_points = new List<TPoint>();
                foreach (var p in bus_points)
                {
                    if (p.velocity < 1.5)
                        new_bus_points.Add(p);
                }
                long_stay_bp[bus_points_kv.Key] = new_bus_points;
            }
            var long_stay_flat = flattenBusesData(long_stay_bp);
            #endregion

            // удаление их из данных
            frwd_flat_vel = frwd_flat_vel.minus(long_stay_flat, 5, 1);

            // ВЫЧИСЛЕНИЕ ТОЧЕК С НЕВЕРНЫМ НАПРАВЛЕНИЕМ СКОРОСТИ
            var wrong_dir_flat = frwd_flat_vel.filter(delegate(TPoint p) {
                var p_angle = Utils.vectorToAngle(p.vel_x, p.vel_y)*180/Math.PI;
                if ((p_angle > -80 && p_angle < 120))
                    return true;
                return false;
            });

            // удалим неверные направления
            frwd_flat_vel = frwd_flat_vel.perPointMinus(wrong_dir_flat);

            // Выделим скопления точек
            // Сделаем map точек в окрестности радиуса 40 с количеством точек больше 35 в одну точку
            var data1_flat = frwd_flat_vel.aggregate(40, 35);            

            // Построим траекторию
            var trajectory_points = getTrajectory(start_tpoint, end_tpoint, frwd_flat_vel);
            var trajectory = new Trajectory(trajectory_points);

            var equidistant_points = new List<TPoint>();
            for (var i = 0; i < 37; i++) {
                equidistant_points.Add(trajectory.pointOnDistanceFromStart(trajectory.length()/38*(i+1)));
            }

            // выделим 37 равноотстоящих точек на траектории
            var equidistant_flat = new TPointData(equidistant_points);     
            
            #region Перемещение равноотстоящего узла в ближайший очаг

            int c = 0;
            // для каждой равнотстоящей точки найдем ближайший очаг точек
            var nearest_to_equidistant_points = new List<TPoint>();
            
            foreach (var p in equidistant_points) { 
                var nearest_to_ed_p = data1_flat.aggregateWithCollectionAroundPoint(p, delegate(List<TPoint> nearPoints){

                    var watch = Stopwatch.StartNew();
                    Console.WriteLine("Точек: {0}", nearPoints.Count);
                    // найдем точечные очаги в данной окрестности и выделим центры этих мини-очагов
                    var near_centers = new Dictionary<int, TPoint>();

                    var checked_points = new List<TPoint>();

                    int max_points_around_count = 0;
                    TPoint max_points_around_center = null;

                    foreach (var np in nearPoints) {
                        if (np == null)
                            throw new NullReferenceException();

                        bool check_this_points = true;
                        foreach (var cp in checked_points) { 
                            if(Utils.distanceBetween(cp, np) < 20){
                                check_this_points = false;
                                break;
                            }                                
                        }
                        if (!check_this_points)
                            continue;

                        checked_points.Add(np);

                        var watch1 = Stopwatch.StartNew();
                        var center = data1_flat.aggregateWithCollectionAroundPoint(np, delegate(List<TPoint> stopPoints) {
                            if (stopPoints.Count == 0)
                                return null;

                            double sum_x = 0, sum_y = 0;
                            foreach (var sp in stopPoints) {
                                sum_x += sp.x;
                                sum_y += sp.y;
                            }

                            var avg_p = new TPoint(-1, sum_x / stopPoints.Count, sum_y / stopPoints.Count, "");

                            if(stopPoints.Count > max_points_around_count){
                                max_points_around_count = stopPoints.Count;
                                max_points_around_center = avg_p;
                            }                                
                            return avg_p;
                        }, 30);
                        watch1.Stop();
                        var elapsedSec1 = (double)watch1.ElapsedMilliseconds / 1000;
                        Console.WriteLine("Поиск центра мини-очага: {0}сек", elapsedSec1);
                    }

                    watch.Stop();
                    var elapsedSec = watch.ElapsedMilliseconds/1000;
                    Console.WriteLine("Поиск центров мини-очагов: {0}сек", elapsedSec);

                    return max_points_around_center;    
                }, 400);

                if(nearest_to_ed_p != null)
                    nearest_to_equidistant_points.Add(nearest_to_ed_p);
                else
                    nearest_to_equidistant_points.Add(p);         
       
                c++;
                Console.WriteLine("Progress: {0} from 37", c);
            }

            var nearest_to_ed_flat = new TPointData(nearest_to_equidistant_points);
            nearest_to_ed_flat.writeToFile("nearest_to_ed_6Jan16.txt");
            
            #endregion


            /* VISUALIZE */

            var vs = new Visualizer(new List<DrawData>() {               
               raw_flat.getDrawData(Color.FromArgb(100, 0, 0, 0)),

               trajectory.getTPointData().getDrawData(Utils.colorAlpha(Color.Violet, 50), 15, null, false, true),

               equidistant_flat.getDrawData(Color.Green, 50),
               nearest_to_ed_flat.getDrawData(Color.DarkOrange, 50),

               frwd_flat_vel.getDrawData(Color.Blue, 2, null, false),
               data1_flat.getDrawData(Color.Red, 10, null, false),

               labeled_points,
            });

            
            vs.run();
        }


#region Helper funcs
        private TPointData flattenBusesData(Dictionary<string, List<TPoint>> buses_points) {
            var points = new List<TPoint>();
            foreach (var kv in buses_points)
            {
                foreach (var p in kv.Value)
                {
                    points.Add(p);
                }
            }
            points.Sort((x, y) => x.time.CompareTo(y.time));
            return new TPointData(points);
        }

        private List<TPoint> getTrajectory(TPoint start_tpoint, TPoint end_tpoint, TPointData data) {
            List<TPoint> trajectory_points;
            if ((trajectory_points = DataProvider.getTPointsFromCache("trajectory.txt")) != null) {
                return trajectory_points;
            }

            trajectory_points = new List<TPoint>();

            var begin_p = start_tpoint;
            
            trajectory_points.Add(begin_p);

            var current_point = begin_p;

            var default_aperture = 80;
            var variable_aperture = default_aperture;

            Random r = new Random();
            var MAX_TRAJ = 30000;
            // пока не достигнем Финиша
            while (Math.Sqrt(
                                Math.Pow(current_point.x - end_tpoint.x, 2)
                                + Math.Pow(current_point.y - end_tpoint.y, 2)
                            )
                            > 50 && trajectory_points.Count < MAX_TRAJ)
            {
                // найдём, куда направлен средний вектор скорости в данной окрестности, и определим так следующую точку
                var next_point = data.aggregateWithCollectionAroundPoint(current_point, delegate(List<TPoint> points_around)
                {
                    // sums for avrg counting
                    double angle_sum = 0;
                    double velval_sum = 0;
                    double x_sum = 0, y_sum = 0;

                    int aperture_points_count = 0;

                    foreach (var p in points_around)
                    {
                        if (p != null)
                        {
                            angle_sum += Utils.vectorToAngle(p.vel_x, p.vel_y);
                            x_sum += p.x;
                            y_sum += p.y;

                            velval_sum += p.velocity;
                            aperture_points_count++;
                        }
                    }

                    bool calculate_next_point = true;
                    if (aperture_points_count == 0)
                    {
                        variable_aperture += 50;
                        calculate_next_point = false;
                    }
                    else if (aperture_points_count > 100)
                    {
                        variable_aperture = default_aperture;
                    }

                    if (calculate_next_point)
                    {
                        // update current coordinates
                        current_point.x = x_sum / aperture_points_count;
                        current_point.y = y_sum / aperture_points_count;

                        double avg_vel_val = velval_sum / aperture_points_count;
                        double avg_angle = angle_sum / aperture_points_count;

                        double STEP = 80;
                        var new_tp = new TPoint(-1,
                                                current_point.x + STEP * Math.Cos(avg_angle),
                                                current_point.y + STEP * Math.Sin(avg_angle),
                                                "");

                        // check if p is already in trajectory
                        foreach (var p in trajectory_points)
                        {
                            if (trajectory_points.Contains(p))
                            {
                                variable_aperture += r.Next(50, 200);
                                break;
                            }
                        }

                        return new_tp;
                    }

                    return current_point;

                }, variable_aperture);

                Console.WriteLine("APERTURE: {0}", variable_aperture);
                Console.WriteLine("TRAJ: {0}", next_point);
                if (next_point != null)
                {
                    if (!trajectory_points.Contains(next_point))
                        trajectory_points.Add(next_point);
                    current_point = next_point;
                }
                else
                {
                    Console.WriteLine("NULL");
                }
            }

            DataProvider.cacheTPoints(trajectory_points, "trajectory.txt");
            return trajectory_points;
        }
    }
#endregion

}
