﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace transport_task
{


    

    class Visualizer
    {

        private const double DISPLAY_QUALITY = 0.3;
        private const bool SAVE_IMAGE = false;

        private GraphicsForm gf;

        private List<DrawData> drawData;

        private int minX, maxX, minY, maxY;
        private Size bitmapSize;
        private GPoint bitmapOffset;


        public Visualizer(List<DrawData> _drawData) {
            drawData = _drawData;

            gf = new GraphicsForm();
            
        }

        public void run() {
            var gf = new GraphicsForm();
            calculateBitmapSizeAndOffset();
            Console.WriteLine("Bitmap real size: {0}x{1}", bitmapSize.Width, bitmapSize.Height);

            var image = draw();
            image = Utils.lower_resolution(image, DISPLAY_QUALITY);            
            gf.setBitmap(image);
            gf.globalStartOffset = bitmapOffset;
            gf.bitmap_quality_zoom = DISPLAY_QUALITY;

            gf.startPoint = new GPoint(-1174, -4762);
            gf.zoom = 0.34;

            Application.Run(gf);
        }

        public Bitmap draw(){
            // collect garbage memory
            GC.Collect();

            Bitmap bitmap;
            bitmap = new Bitmap(bitmapSize.Width, bitmapSize.Height);
            
           
            using (Graphics g = Graphics.FromImage(bitmap))
            {
                // Flip the Y-Axis
              //  g.ScaleTransform(1.0F, -1.0F);

                // Translate the drawing area accordingly
               // g.TranslateTransform(0.0F, (float)bitmapSize.Height);

                // make it white
                g.FillRectangle(new SolidBrush(Color.White), 0, 0, bitmap.Width, bitmap.Height);

                
                foreach(DrawData dd in drawData){
                    GPoint prev_point = null;                    
                    foreach (GPoint p in dd.points)
                    {
                       // Console.WriteLine(p.ToString());
                        drawDot(g, p, dd.drawVectors);
                        if (prev_point != null && dd.connectPoints)
                            drawConnection(g, prev_point, p);
                        prev_point = p;
                    }
                }
                                
            }


            if (SAVE_IMAGE) {
                SaveFileDialog dialog = new SaveFileDialog();
                dialog.FileName = "test.png";
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    bitmap.Save(dialog.FileName, ImageFormat.Png);
                }
            }
            

            return bitmap;             
        }

        private void drawConnection(Graphics gr, GPoint p1, GPoint p2){
            GPoint draw_p1 = p1 + bitmapOffset;
            GPoint draw_p2 = p2 + bitmapOffset;
            // reverse y-axis
            draw_p1.y = bitmapSize.Height - draw_p1.y;
            draw_p1.vy = -draw_p1.vy;

            draw_p2.y = bitmapSize.Height - draw_p2.y;
            draw_p2.vy = -draw_p2.vy;

            int point_size = draw_p1.size + 1;
            SolidBrush brush = new SolidBrush(draw_p1.color);
            var pen = new Pen(brush);
            pen.Width = draw_p1.size/3;

            // draw dot
            gr.DrawLine(pen, new Point(draw_p1.x, draw_p1.y), new Point(draw_p2.x, draw_p2.y));
        }

        private void drawDot(Graphics gr, GPoint global_p, bool drawVector)
        {
            GPoint draw_p = global_p + bitmapOffset;
            // reverse y-axis
            draw_p.y = bitmapSize.Height - draw_p.y;
            draw_p.vy = -draw_p.vy;

            int point_size = draw_p.size + 1;
            SolidBrush brush = new SolidBrush(draw_p.color);

            var dot_x = draw_p.x - point_size / 2;
            var dot_y = draw_p.y - point_size / 2;

            // draw dot
            gr.FillEllipse(brush, dot_x, dot_y, point_size, point_size);
            if (global_p.label != null) {
                gr.DrawString(global_p.label, new Font("Helvetica", 20), Brushes.Black, (draw_p+new GPoint(20, 20)).ToPoint());
            }

            if (drawVector) {
                if (draw_p.vx != 0 || draw_p.vy != 0)
                {
                    // draw vector
                    var vector_end_size = draw_p.size * 2;
                    var pen = new Pen(brush);
                    pen.Width = draw_p.size;
                    gr.DrawLine(pen, new Point(dot_x, dot_y), new Point(dot_x + draw_p.vx, dot_y + draw_p.vy));
                    gr.FillEllipse(new SolidBrush(Color.FromArgb(draw_p.color.A, 255, draw_p.color.G, draw_p.color.B)), dot_x + draw_p.vx - vector_end_size / 2, dot_y + draw_p.vy - vector_end_size / 2, vector_end_size, vector_end_size);
                }
            }            
        }


        private void calculateBitmapSizeAndOffset() {
            minX = minY = int.MaxValue;
            maxX = maxY = int.MinValue;

            bitmapOffset = new GPoint(0, 0);

            foreach (DrawData dd in drawData) {
                foreach (GPoint p in dd.points)
                {
                    var gp = new GPoint(p.x, p.y);
                    if (gp.x > maxX)
                        maxX = gp.x;
                    if (gp.x < minX)
                        minX = gp.x;

                    if (gp.y > maxY)
                        maxY = gp.y;
                    if (p.y < minY)
                        minY = gp.y;
                }
            }
            

            if (minX < 0) {
                maxX += Math.Abs(minX);
                bitmapOffset.x = Math.Abs(minX);
                minX = 0;
            }

            if (minY < 0)
            {
                maxY += Math.Abs(minY);
                bitmapOffset.y = Math.Abs(minY);
                minY = 0;
            }

            bitmapSize = new Size(maxX, maxY);
        }


        
    }
}
