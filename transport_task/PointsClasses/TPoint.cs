﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace transport_task.PointsClasses
{
    public class TPoint
    { // transport point
        public int time;
        public double x, y;
        public double velocity;
        public double vel_x, vel_y;
        public double acceleration;
        public string id;

        public TPoint(int _time, double _x, double _y, string _id)
        {
            time = _time;
            x = _x;
            y = _y;
            id = _id;

            velocity = 0;
            vel_x = vel_y = 0;
            acceleration = 0;
        }

        public double norm()
        {
            return Math.Sqrt(x * x + y * y);
        }

        public GPoint ToGPoint() {
            return new GPoint(this);
        }


        public override string ToString()
        {
            return string.Format("([{0}], ({1}, {2}), {3})", Utils.UnixTimeStampToDateTime((double)time), x, y, id);
        }
    }
}
