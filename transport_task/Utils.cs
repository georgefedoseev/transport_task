﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using transport_task.PointsClasses;

namespace transport_task
{

    public class Vector2{
        public double x, y;

        public Vector2(double _x, double _y){
            x = _x;
            y = _y;
        }
    }

    public class Utils
    {

        public static double distanceBetween(TPoint p1, TPoint p2) {
            return Math.Sqrt(
                                Math.Pow(p1.x - p2.x, 2)
                                + Math.Pow(p1.y - p2.y, 2)
                            );
        }

        public static Color colorAlpha(Color c, int alpha) {
            return Color.FromArgb(alpha, c.R, c.G, c.B);
        }

        public static double vectorToAngle(double x, double y) {
            var angle = x != 0 ? Math.Atan(y / x) + (x > 0 ? 0 : Math.PI) : 0;
            if (angle < 0)
                return angle + Math.PI * 2;
            return angle;         
        }

        public static Vector2 angleToVector(double a)
        {
            return new Vector2(Math.Cos(a), Math.Sin(a));
        }

        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }

        public static void Log(string logMessage)
        {
            using (StreamWriter w = File.AppendText("log.txt"))
            {   
                w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                    DateTime.Now.ToLongDateString());
                
                w.WriteLine("  :{0}", logMessage);
                w.WriteLine ("-------------------------------");
            }
        }

        public static Bitmap lower_resolution(Bitmap image, double factor)
        {
            int new_width = (int)(image.Width * factor),
                new_height = (int)(image.Height * factor);
            Bitmap lr_bitmap = new Bitmap(new_width, new_height);
            var g = Graphics.FromImage(lr_bitmap);
            g.DrawImage(image, new RectangleF(0, 0, new_width, new_height));
            return lr_bitmap;
        }

        public static Bitmap removeAlpha(Bitmap source){            
            Bitmap result = new Bitmap(source.Size.Width, source.Size.Height);
            Graphics g = Graphics.FromImage(result);
            g.DrawRectangle(new Pen(new SolidBrush(Color.White)), 0, 0, result.Width, result.Height);
            g.DrawImage(source, 0, 0);
            return result;
        }

        public static double secondsSince1970(){
            TimeSpan span= DateTime.Now.Subtract(new DateTime(1970,1,1,0,0,0, DateTimeKind.Utc));
            return span.TotalSeconds;
        }
    }

    public sealed class Nothing
    {
        private Nothing() { }
        private readonly static Nothing atAll = new Nothing();
        public static Nothing AtAll { get { return atAll; } }
    }

    
}
