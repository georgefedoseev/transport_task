﻿namespace transport_task
{
    partial class GraphicsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.stop_number = new System.Windows.Forms.Label();
            this.next_button = new System.Windows.Forms.Button();
            this.prev_button = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.current_stop_coords = new System.Windows.Forms.Label();
            this.write_to_file_btn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tags_count = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // stop_number
            // 
            this.stop_number.AutoSize = true;
            this.stop_number.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.stop_number.Location = new System.Drawing.Point(1273, 51);
            this.stop_number.Name = "stop_number";
            this.stop_number.Size = new System.Drawing.Size(121, 24);
            this.stop_number.TabIndex = 0;
            this.stop_number.Text = "stop_number";
            this.stop_number.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.stop_number.Visible = false;
            // 
            // next_button
            // 
            this.next_button.Location = new System.Drawing.Point(1400, 38);
            this.next_button.Name = "next_button";
            this.next_button.Size = new System.Drawing.Size(71, 23);
            this.next_button.TabIndex = 1;
            this.next_button.Text = "Next";
            this.next_button.UseVisualStyleBackColor = true;
            this.next_button.Visible = false;
            this.next_button.Click += new System.EventHandler(this.next_button_Click);
            // 
            // prev_button
            // 
            this.prev_button.Location = new System.Drawing.Point(1192, 38);
            this.prev_button.Name = "prev_button";
            this.prev_button.Size = new System.Drawing.Size(71, 23);
            this.prev_button.TabIndex = 2;
            this.prev_button.Text = "Prev";
            this.prev_button.UseVisualStyleBackColor = true;
            this.prev_button.Visible = false;
            this.prev_button.Click += new System.EventHandler(this.prev_button_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(1302, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 24);
            this.label1.TabIndex = 3;
            this.label1.Text = "Stop:";
            this.label1.Visible = false;
            // 
            // current_stop_coords
            // 
            this.current_stop_coords.AutoSize = true;
            this.current_stop_coords.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.current_stop_coords.Location = new System.Drawing.Point(1242, 86);
            this.current_stop_coords.Name = "current_stop_coords";
            this.current_stop_coords.Size = new System.Drawing.Size(182, 24);
            this.current_stop_coords.TabIndex = 4;
            this.current_stop_coords.Text = "current_stop_coords";
            this.current_stop_coords.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.current_stop_coords.Visible = false;
            // 
            // write_to_file_btn
            // 
            this.write_to_file_btn.Location = new System.Drawing.Point(633, 12);
            this.write_to_file_btn.Name = "write_to_file_btn";
            this.write_to_file_btn.Size = new System.Drawing.Size(117, 23);
            this.write_to_file_btn.TabIndex = 5;
            this.write_to_file_btn.Text = "Save To File";
            this.write_to_file_btn.UseVisualStyleBackColor = true;
            this.write_to_file_btn.Visible = false;
            this.write_to_file_btn.Click += new System.EventHandler(this.write_to_file_btn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(1371, 297);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 24);
            this.label2.TabIndex = 7;
            this.label2.Text = "Tags:";
            this.label2.Visible = false;
            // 
            // tags_count
            // 
            this.tags_count.AutoSize = true;
            this.tags_count.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tags_count.Location = new System.Drawing.Point(1371, 330);
            this.tags_count.Name = "tags_count";
            this.tags_count.Size = new System.Drawing.Size(101, 24);
            this.tags_count.TabIndex = 6;
            this.tags_count.Text = "tags_count";
            this.tags_count.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.tags_count.Visible = false;
            // 
            // GraphicsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1504, 730);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tags_count);
            this.Controls.Add(this.write_to_file_btn);
            this.Controls.Add(this.current_stop_coords);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.prev_button);
            this.Controls.Add(this.next_button);
            this.Controls.Add(this.stop_number);
            this.Name = "GraphicsForm";
            this.Text = "Graphics Form";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label stop_number;
        private System.Windows.Forms.Button next_button;
        private System.Windows.Forms.Button prev_button;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label current_stop_coords;
        private System.Windows.Forms.Button write_to_file_btn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label tags_count;



    }
}

