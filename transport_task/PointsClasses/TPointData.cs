﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using transport_task.PointsClasses;

namespace transport_task
{
    public class TPointData : DotData<TPoint>
    {
        public TPointData(List<TPoint> _points):base(_points) {}
        public TPointData(TPoint _point) : base(new List<TPoint>() { _point }) { }



        public void writeToFile(string filename) {
            File.Delete(filename);
            using (StreamWriter w = File.AppendText(filename))
            {
                foreach (var p in points) {
                    w.WriteLine("{0} {1}", p.x, p.y);
                }
            }
        }

        public new TPointData filter(Func<TPoint, bool> _check_func)
        {
            return new TPointData(base.filter(_check_func).points);
        }

        public DrawData getDrawData(Color _color, int _point_size = 1, string _label = null, bool _drawVectors = false, bool _connectPoints = false)
        {
            return base.getDrawData(delegate(TPoint p){
                double VECTOR_SIZE_MLTPL = 10;
                var gpoint = new GPoint(
                        p.x,
                        p.y,
                        _color,
                        _label,
                        (int)(VECTOR_SIZE_MLTPL*p.vel_x),
                        (int)(VECTOR_SIZE_MLTPL*p.vel_y),
                        _point_size
                    );
                
                return gpoint;
            }, _drawVectors, _connectPoints);
        }

        public TPointData aggregate(int APERTURE_WIDTH, int APERTURE_SCORE_THRESHOLD)
        {            
            return many_operation(this, new TPointData(new List<TPoint>()), (bool x, bool y) =>
            {
                return x;
            }, APERTURE_WIDTH, APERTURE_SCORE_THRESHOLD);
        }


        public TPointData crossWith(TPointData b, int APERTURE_WIDTH = 20, int APERTURE_SCORE_THRESHOLD = 1)
        {
            return many_operation(this, b, (bool x, bool y) =>
            {
                return x && y;
            }, APERTURE_WIDTH, APERTURE_SCORE_THRESHOLD);
        }

        public TPointData minus(TPointData b, int APERTURE_WIDTH = 20, int APERTURE_SCORE_THRESHOLD = 1)
        {
            return many_operation(this, b, (bool x, bool y) =>
            {
                return /*x &&*/ !y;
            }, APERTURE_WIDTH, APERTURE_SCORE_THRESHOLD);
        }

        public TPointData perPointMinus(TPointData minus_data) {
            var res_points = new List<TPoint>(this.points);
            foreach (var mp in minus_data.points) {
                res_points.Remove(mp);
            }

            return new TPointData(res_points);
        }

        private static TPointData many_operation(TPointData a, TPointData b, Func<bool, bool, bool> compare_func, int APERTURE_WIDTH, int APERTURE_SCORE_THRESHOLD)
        {

            var result = new List<TPoint>();

            // find min and max
            int minX, maxX, minY, maxY;
            minX = minY = int.MaxValue;
            maxX = maxY = int.MinValue;


            foreach (var p in a.points)
            {
                if (p.x > maxX)
                    maxX = (int)p.x;
                if (p.x < minX)
                    minX = (int)p.x;

                if (p.y > maxY)
                    maxY = (int)p.y;
                if (p.y < minY)
                    minY = (int)p.y;
            }

            foreach (var p in b.points)
            {
                if (p.x > maxX)
                    maxX = (int)p.x;
                if (p.x < minX)
                    minX = (int)p.x;

                if (p.y > maxY)
                    maxY = (int)p.y;
                if (p.y < minY)
                    minY = (int)p.y;
            }


            int data_width = (int)(maxX - minX) + 2;
            int data_height = (int)(maxY - minY) + 2;

            int data_bias_x = -(int)minX;
            int data_bias_y = -(int)minY;

            /* PREPARE arrA */
            /* CREATE 2d array a la bitmap */
            short[,] arrA = new short[data_height, data_width];

            foreach (var p in a.points)
            {
                arrA[(data_bias_y + (int)p.y), (data_bias_x + (int)p.x)]++;
            }

            /* PREPARE arrB */
            /* CREATE 2d array a la bitmap */
            short[,] arrB = new short[data_height, data_width];

            foreach (var p in b.points)
            {
                arrB[(data_bias_y + (int)p.y), (data_bias_x + (int)p.x)]++;
            }




            int c = 0;
            /* CHECK AROUND EACH POINT */
            foreach (var p in a.points)
            {
                c++;

                int x = (data_bias_x + (int)p.x);
                int y = (data_bias_y + (int)p.y);

                int start_x = x - APERTURE_WIDTH / 2;
                int start_y = y - APERTURE_WIDTH / 2;

                int apertureA_sum = 0;
                for (int i = 0; i < APERTURE_WIDTH; i++)
                {
                    for (int j = 0; j < APERTURE_WIDTH; j++)
                    {
                        int cx = start_x + i;
                        int cy = start_y + j;

                        if (cx >= 0 && cx < data_width
                            && cy >= 0 && cy < data_height)
                        {
                            apertureA_sum += arrA[cy, cx];
                        }
                    }
                }

                int apertureB_sum = 0;
                for (int i = 0; i < APERTURE_WIDTH; i++)
                {
                    for (int j = 0; j < APERTURE_WIDTH; j++)
                    {
                        int cx = start_x + i;
                        int cy = start_y + j;

                        if (cx >= 0 && cx < data_width
                            && cy >= 0 && cy < data_height)
                        {
                            apertureB_sum += arrB[cy, cx];
                        }
                    }
                }


                if (compare_func(apertureA_sum >= APERTURE_SCORE_THRESHOLD, apertureB_sum >= APERTURE_SCORE_THRESHOLD))
                {
                    // Console.WriteLine("sumA: {0}; sumB: {1}", apertureA_sum, apertureB_sum);
                    result.Add(p);
                }

            }

            return new TPointData(result);
        }



        public TPointData aggregateWithCollection(Func<List<TPoint>, TPoint> collect_func, int APERTURE_WIDTH, bool for_each_point = true)
        {
            return agregate_with_collection(this, collect_func, APERTURE_WIDTH, for_each_point);
        }


        public TPoint aggregateWithCollectionAroundPoint(TPoint p, Func<List<TPoint>, TPoint> collect_func, int APERTURE_WIDTH)
        {
            return agregate_with_collection_around_point(p, this, collect_func, APERTURE_WIDTH);
        }

        private static TPointData agregate_with_collection(TPointData a, Func<List<TPoint>, TPoint> collect_func, int APERTURE_WIDTH, bool for_each_point = true)
        {

            var result = new List<TPoint>();

            // find min and max
            int minX, maxX, minY, maxY;
            minX = minY = int.MaxValue;
            maxX = maxY = int.MinValue;


            foreach (var p in a.points)
            {
                if (p.x > maxX)
                    maxX = (int)p.x;
                if (p.x < minX)
                    minX = (int)p.x;

                if (p.y > maxY)
                    maxY = (int)p.y;
                if (p.y < minY)
                    minY = (int)p.y;
            }

          

            int data_width = (int)(maxX - minX) + 2;
            int data_height = (int)(maxY - minY) + 2;

            int data_bias_x = -(int)minX;
            int data_bias_y = -(int)minY;

            /* PREPARE arrA */
            /* CREATE 2d array a la bitmap */
            TPoint[,] arrA = new TPoint[data_height, data_width];

            foreach (var p in a.points)
            {
                arrA[(data_bias_y + (int)p.y), (data_bias_x + (int)p.x)] = p;
            }



            List<TPoint> checked_points = new List<TPoint>();

            int c = 0;
            /* AGGREGATE AROUND EACH POINT */
            foreach (var p in a.points)
            {
                c++;

                if (!for_each_point) { 
                
                    // check if it's already checked around
                    bool already_checked = false;
                    foreach (var cp in checked_points) {
                        if ( 
                            Math.Sqrt(
                                Math.Pow(p.x - cp.x, 2)
                                + Math.Pow(p.y - cp.y, 2)
                            )
                            < APERTURE_WIDTH/2) 
                        {
                            already_checked = true;
                            break;
                        }
                    }
                    if (already_checked)
                        continue;
                }

                checked_points.Add(p);


                int x = (data_bias_x + (int)p.x);
                int y = (data_bias_y + (int)p.y);

                int start_x = x - APERTURE_WIDTH / 2;
                int start_y = y - APERTURE_WIDTH / 2;

                List<TPoint> aperture_collectionA = new List<TPoint>();
                aperture_collectionA.Add(p);
                for (int i = 0; i < APERTURE_WIDTH; i++)
                {
                    for (int j = 0; j < APERTURE_WIDTH; j++)
                    {
                        int cx = start_x + i;
                        int cy = start_y + j;

                        if (cx >= 0 && cx < data_width
                            && cy >= 0 && cy < data_height)
                        {
                            if (arrA[cy, cx] != null)
                                aperture_collectionA.Add(arrA[cy, cx]);
                        }
                    }
                }


                var new_p = collect_func(aperture_collectionA);

                if (new_p != null) {
                    // dont let change coordinates. TODO: think if I need this restriction
                    new_p.x = p.x;
                    new_p.y = p.y;

                    result.Add(new_p);
                }
                
            }

            return new TPointData(result);
        }



        private static TPoint agregate_with_collection_around_point(TPoint around_point, TPointData a, Func<List<TPoint>, TPoint> collect_func, int APERTURE_WIDTH)
        {   

            // find min and max
            int minX, maxX, minY, maxY;
            minX = minY = int.MaxValue;
            maxX = maxY = int.MinValue;


            foreach (var p in a.points)
            {
                if (p.x > maxX)
                    maxX = (int)p.x;
                if (p.x < minX)
                    minX = (int)p.x;

                if (p.y > maxY)
                    maxY = (int)p.y;
                if (p.y < minY)
                    minY = (int)p.y;
            }



            int data_width = (int)(maxX - minX) + 2;
            int data_height = (int)(maxY - minY) + 2;

            int data_bias_x = -(int)minX;
            int data_bias_y = -(int)minY;

            /* PREPARE arrA */
            /* CREATE 2d array a la bitmap */
            TPoint[,] arrA = new TPoint[data_height, data_width];

            foreach (var p in a.points)
            {
                arrA[(data_bias_y + (int)p.y), (data_bias_x + (int)p.x)] = p;
            }



            List<TPoint> checked_points = new List<TPoint>();

            int c = 0;
            /* AGGREGATE AROUND  POINT */

            int x = (data_bias_x + (int)around_point.x);
            int y = (data_bias_y + (int)around_point.y);

            int start_x = x - APERTURE_WIDTH / 2;
            int start_y = y - APERTURE_WIDTH / 2;

            List<TPoint> aperture_collectionA = new List<TPoint>();
          //  aperture_collectionA.Add(around_point);
            for (int i = 0; i < APERTURE_WIDTH; i++)
            {
                for (int j = 0; j < APERTURE_WIDTH; j++)
                {
                    int cx = start_x + i;
                    int cy = start_y + j;

                    if (cx >= 0 && cx < data_width
                        && cy >= 0 && cy < data_height)
                    {
                        if (arrA[cy, cx] != null)
                            aperture_collectionA.Add(arrA[cy, cx]);
                    }
                }
            }


            var new_p = collect_func(aperture_collectionA);
                        
            return new_p;            
        }

    }
}
