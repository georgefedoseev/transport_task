﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace transport_task
{
    public class DrawData
    {        
        public List<GPoint> points;
        public bool drawVectors;
        public bool connectPoints;

        public DrawData(List<GPoint> _points, Color? _color = null, int _point_size = -1, bool _drawVectors = false, bool _connectPoints = false)
        {
            points = _points;
            drawVectors = _drawVectors;
            connectPoints = _connectPoints;

            if (_color != null) {
                foreach (var p in points) {
                    p.color = _color.Value;
                }
            }

            if (_point_size >= 0) { 
                foreach (var p in points) {
                    p.size = _point_size;
                }
            }
        }
    }
}
